# protocolSample 样例说明
* 本样例构建一个metadata的protocol数据并送入stream，随后输出至log
* 上传一张jpg格式图片并重命名为test.jpg，在运行目录下执行run.sh。请勿使用大分辨率图片
* 如构建的proto数据正确则可在命令行输出中看到warn登记的日志，以"Output:"开头